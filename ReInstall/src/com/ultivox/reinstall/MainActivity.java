package com.ultivox.reinstall;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MainActivity extends Activity {

	private MyTask mt;
	private Activity context;

    public static String systemNandBank = "";

    final static String LOG_TAG = "ReInstallLogs";

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("ReInstall", "started");
		context = this;
        systemNandBank = getSystemNandBank();
        Log.d(LOG_TAG, systemNandBank);
		mt = new MyTask();
		mt.execute();

	}

	class MyTask extends AsyncTask<Void, Void, Integer> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Log.d("ReInstall", "Begin");
		}

		@Override
		protected Integer doInBackground(Void... params) {
//			String[] command = new String[] { "su", "-c",
//					"mount -o rw,remount /dev/block/nandd /system && busybox cp /mnt/sdcard/Download/UVoxPlayer.apk /system/app/ && chmod 644 /system/app/UVoxPlayer.apk ; mount -o ro,remount /dev/block/nandd /system" };

//            String[] command = new String[] { "su", "-c",
//                    "mount -o rw,remount /dev/block/"+ systemNandBank +" /system && busybox cp /mnt/sdcard/Download/UVoxPlayer.apk /system/app/ && chmod 644 /system/app/UVoxPlayer.apk ; mount -o ro,remount /dev/block/"+ systemNandBank +" /system" };

			String[] command = new String[] { "su", "-c", "cp /mnt/sdcard/Download/UVoxPlayer.apk /system/app/ && chmod 644 /system/app/UVoxPlayer.apk" };

			Log.d("ReInstall", command[0] + " " + command[1] + " " + command[2]);
			Process reInsProc;
			try {
				reInsProc = Runtime.getRuntime().exec(command);
				int res = reInsProc.waitFor();
				Log.d("ReInstall", "Result of reinstall : " + res);
				return res;
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);
			if (result == null) {
				return;
			}
			Intent reboot = new Intent();
			reboot.setClassName("com.ultivox.rebootapp", "com.ultivox.rebootapp.MainActivity");
			reboot.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(reboot);
			Log.d("ReInstall", "End");
		}
	}

    public static String getSystemNandBank() {

        try {
            Process process = Runtime.getRuntime().exec("cat /proc/partitions");

            // Reads stdout.

            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            int read;
            char[] buffer = new char[4096];
            StringBuffer output = new StringBuffer();
            while ((read = reader.read(buffer)) > 0) {
                output.append(buffer, 0, read);
            }
            reader.close();

            // Waits for the command to finish.
            process.waitFor();

            if (output.toString().contains("nand"))
                return "nandd";                             //for Allwiner processors
            if (output.toString().contains("mtdblock"))
                return "mtdblock8";                         //for Rockchip processors
            if (output.toString().contains("mmcblk"))
                return "mmcblk0p8";                         //for MTK processors

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "nandd";
    }
}
